<?php
require_once "../modelos/Venta.php";
if(strlen(session_id()) <1)
    session_start();
$venta = new Venta();

$idventa = isset($_POST["idventa"])?limpiarCadena($_POST["idventa"]):"";
$idcliente = isset($_POST["idcliente"])?limpiarCadena($_POST["idcliente"]):"";
$idusuario = $_SESSION["idusuario"];
$tipo_comprobante = isset($_POST["tipo_comprobante"])?limpiarCadena($_POST["tipo_comprobante"]):"";
$serie_comprobante = isset($_POST["serie_comprobante"])?limpiarCadena($_POST["serie_comprobante"]):"";
$num_comprobante = isset($_POST["num_comprobante"])?limpiarCadena($_POST["num_comprobante"]):"";
$fecha_hora = isset($_POST["fecha_hora"])?limpiarCadena($_POST["fecha_hora"]):"";
$impuesto = isset($_POST["impuesto"])?limpiarCadena($_POST["impuesto"]):"";
$total_venta = isset($_POST["total_venta"])?limpiarCadena($_POST["total_venta"]):"";


switch($_GET["op"]){
    case 'guardaryeditar':
        if(empty($idventa)){
            $rspta = $venta->insertar($idcliente,$idusuario,$tipo_comprobante,$serie_comprobante,
            $num_comprobante,$fecha_hora,$impuesto,$total_compra,$_POST["idarticulo"],
            $_POST["cantidad"],$_POST["precio_venta"],$_POST["descuento"]);
            echo $rspta? "Venta Registrada":"No se pudieron registrar todos los datos de la venta";
        }
        else{
        }
    break;
    case 'anular':
        $rspta = $venta->anular($idventa);
        echo $rspta? " Venta anulada":"Venta no se pudo anular";
    break;
    case 'mostrar':
        $rspta = $venta->mostrar($idventa);
        echo json_encode($rspta);
    break;
    case "listarDetalle":
    $id=$_GET["id"];
    $rspta= $venta->listarDetalle($id);
    $total=0.0;
    echo ' <thead style="background-color: #A9D0F5">
    <th>Opciones</th>
    <th>Articulo</th>
    <th>Cantidad</th>
    <th>Precio Venta</th>
    <th>Descuento</th>
    <th>Subtotal</th>
</thead>';
    while($reg=$rspta->fetch_object()){
        echo '<tr class="fila">
             <td></td>
             <td>'.$reg->nombre.'</td>
             <td>'.$reg->cantidad.'</td>
             <td>'.$reg->precio_venta.'</td>
             <td>'.$reg->descuento.'</td>
             <td>'.$reg->precio_venta*$reg->cantidad.'</td>
            </tr>';
            $total += $reg->precio_venta*$reg->cantidad;
    }
    echo '<tfoot>
    <th>TOTAL</th>
    <th></th>
    <th></th>
    <th></th>
    <th></th>
    <th><h4 id="total">Bs./ '.$total.'</h4>
    <input type="hidden" name="total_venta" id="total_venta"></th>
</tfoot>';
    break;
    case 'listar':
        $rspta = $venta->listar();
        $data= Array();

        while($reg=$rspta->fetch_object()){
            $data[]=array(
                "0"=>($reg->estado=='Aceptado')? '<button class="btn btn-warning" onclick="mostrar('.$reg->idventa.')"><i class="fa fa-eye"></i></button>'.
                ' <button class="btn btn-danger" onclick="anular('.$reg->idventa.')"><i class="fa fa-close"></i></button>':
                '<button class="btn btn-warning" onclick="mostrar('.$reg->idventa.')"><i class="fa fa-pencil"></i></button>',
                "1"=>$reg->fecha,
                "2"=>$reg->cliente,
                "3"=>$reg->usuario,
                "4"=>$reg->tipo_comprobante,
                "5"=>$reg->serie_comprobante."-".$reg->num_comprobante,
                "6"=>$reg->total_venta,
                "7"=>($reg->estado=='Aceptado')? '<span class="label bg-green">Aeptado</span>': '<span class="label bg-red">Anulado</span>'
            );
        }
        $results = Array(
            "sEcho"=>1,//informacion al data table
            "iTotalRecords"=>count($data),//total registros datatable
            "iTotalDisplayRecords"=>count($data),//total de registros 
            "aaData"=>$data
        );
        echo json_encode($results);
        
    break;
    case "selectCliente":
        require_once "../modelos/Persona.php";
        $persona = new Persona();
        $rspta = $persona->listarc();
        while($reg= $rspta->fetch_object()){
            echo '<option value='.$reg->idpersona.'>'.$reg->nombre.'</option>';
        }
    break;
    case "listarArticulos":
        require_once "../modelos/Articulo.php";
        $articulo= new Articulo();
        $rspta = $articulo->listarActivos();
        $data= Array();

        while($reg=$rspta->fetch_object()){
            $data[]=array(
                "0"=>'<button class="btn btn-warning" onclick="agregarDetalle('.$reg->idarticulo.',\''.$reg->nombre.'\')">
                <span class="fa fa-plus"></span></button>',
                "1"=>$reg->nombre,
                "2"=>$reg->categoria,
                "3"=>$reg->codigo,
                "4"=>$reg->stock,
                "5"=>'<img src="../files/articulos/'.$reg->imagen.'" height="50px" width="50px">',
                
            );
        }
        $results = Array(
            "sEcho"=>1,//informacion al data table
            "iTotalRecords"=>count($data),//total registros datatable
            "iTotalDisplayRecords"=>count($data),//total de registros 
            "aaData"=>$data
        );
        echo json_encode($results);
    break;

}


?>
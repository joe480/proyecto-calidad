<?php
ob_start();
session_start();
if(!isset($_SESSION["nombre"])){
  header("Location: login.html");
}
else{
require "header.php";
if($_SESSION["ventas"]==1)
{
?>
<!--Contenido-->
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            <div class="row">
              <div class="col-md-12">
                  <div class="box">
                    <div class="box-header with-border">
                          <h1 class="box-title">Ventas <button class="btn btn-success"id="btnagregar" onclick="mostrarForm(true)"><i class="fa fa-plus-circle"></i> Agregar</button></h1>
                        <div class="box-tools pull-right">
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <!-- centro -->
                    <div class="panel-body table-responsive"  id="listadoRegistros">
                        <table id="tbllistado" class="table  table-striped table-bordered table-condensed table-hover">
                          <thead>
                            <th>Opciones</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Usuario</th>
                            <th>Documento</th>
                            <th>Numero</th>
                            <th>Total Venta</th>
                            <th>Estado</th>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                            <th>Opciones</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Usuario</th>
                            <th>Documento</th>
                            <th>Numero</th>
                            <th>Total Venta</th>
                            <th>Estado</th>
                          </tfoot>
                        </table>
                    </div>
                    <div class="panel-body" style="height: 400px;" id="formularioRegistros">
                        <form name="formulario" id="formulario" method="POST">
                          <div class="form-group col-lg-8 col-md-8 col-sm-8 col-xs-12"
                          >
                            <label >Cliente(*):</label>
                            <input type="hidden" name="idventa" id="idventa">
                           <select name="idcliente" id="idcliente" class="form-control selectpicker"
                           data-live-search=true required>

                           </select>
                          </div>
                          <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12"
                          >
                            <label >fecha(*):</label>
                            <input type="date" class="form-control" name="fecha_hora" id="fecha_hora"
                             required="">
                          </div>
                          <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
                              <label >Tipo Comprobante(*):</label>
                              <select name="tipo_comprobante" id="tipo_comprobante" 
                              class="form-control selectpicker" required="">
                                  <option value="Boleta">Boleta</option>
                                  <option value="Factura">Factura</option>
                                  <option value="Ticket">Ticket</option>
                              </select>
                          </div >
                            <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <label >Serie:</label>
                                <input type="text" name="serie_comprobante" id="serie_comprobante"
                                class="form-control" maxlength="7" placeholder="Serie">
                             </div>
                             <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <label >Número:</label>
                                <input type="text" name="num_comprobante" id="num_comprobante"
                                class="form-control" maxlength="10" placeholder="Numero" required="">
                             </div>
                             <div class="form-group col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <label >Impuesto(*):</label>
                                <input type="text" name="impuesto" id="impuesto"
                                class="form-control" required="">
                             </div >
                             <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                             <a data-toggle="modal" href="#myModal">
                                 <button id="btnAgregarArt" type="button" class="btn btn-primary">
                                    <span class="fa fa-plus"></span>Agregar Articulo</button>
                             </a>
                             </div>

                             <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table class="table table-stiped table-bordered table-condensed
                                table-hover" id="detalles">
                                    <thead style="background-color: #A9D0F5">
                                        <th>Opciones</th>
                                        <th>Articulo</th>
                                        <th>Cantidad</th>
                                        <th>Precio Venta</th>
                                        <th>Descuento</th>
                                        <th>Subtotal</th>
                                    </thead>
                                    <tfoot>
                                        <th>TOTAL</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><h4 id="total">Bs/. 0.00</h4>
                                        <input type="hidden" name="total_venta" id="total_venta"></th>
                                    </tfoot>
                                    <tbody>

                                    </tbody>
                                </table>
                             </div>
                          <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <button class="btn btn-primary" type="submit" id="btnGuardar">
                            <i class="fa fa-save"></i>Guardar</button>
                          <button id="btnCancelar" class="btn btn-danger" onclick="cancelarForm()" type="button">
                          <i class="fa fa-arrow-circle-left"></i>Cancelar</button>
                          </div>
                        </form>
                    </div>
                    <!--Fin centro -->
                  </div><!-- /.box -->
              </div><!-- /.col -->
          </div><!-- /.row -->
      </section><!-- /.content -->

    </div><!-- /.content-wrapper -->
  <!--Fin-Contenido-->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
   aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 65% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" 
                aria-hidden="true">&times; </button>
                <h4 class="modal-title">Seleccione un Articulo</h4>
            </div>
            <div class="modal-body">
                <table id="tblarticulos" class="table table-striped table-bordered
                table-condensed table-hover">
                <thead>
                    <th>Opciones</th>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Codigo</th>
                    <th>Stock</th>
                    <th>Imagen</th>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <th>Opciones</th>
                    <th>Nombre</th>
                    <th>Categoria</th>
                    <th>Codigo</th>
                    <th>Stock</th>
                    <th>Imagen</th>
                </tfoot>
                </table>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
  </div>
  <?php
}
else
{
  require "noacceso.php";
}
require "footer.php";
?>
<script type="text/javascript" src="scripts/ingreso.js"></script>
<?php
}
ob_end_flush();
?>
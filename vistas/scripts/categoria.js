var tabla;
function init(){
    mostrarForm(false)
    listar();

    $("#formulario").on("submit",function(e){
        guardaryeditar(e);
    })
}
function limpiar(){
    $("#idcategoria").val("");
    $("#nombre").val("");
    $("#descripcion").val("");
}
function mostrarForm(flag){
    limpiar();
    if(flag){
        $("#listadoRegistros").hide();
        $("#formularioRegistros").show();
        $("#btnGuardar").prop("disabled",false);
        $("#btnagregar").hide();
    }
    else{
        $("#listadoRegistros").show();
        $("#formularioRegistros").hide();
        $("#btnagregar").show();
    }
}

function cancelarForm(){
    limpiar();
    mostrarForm(false);
}

function listar(){
    tabla=$('#tbllistado').dataTable(
        {
        "aProcessing":true,//Activamos el procesamiento de dataTables
        "aServerSide":true,//Paginacion y filtrado realizados por el servidor
        dom: 'Bfrtip',//Definimos los elementos de control de la tabla
        buttons:[
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":
                {
                    url:'../ajax/categoria.php?op=listar',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "bDestroy":true,
        "iDisplayLength":5,//Paginacion
        "order":[[0,"desc"]]        
    }).DataTable();
}

function guardaryeditar(e){
    e.preventDefault(); //No se activara la accion predeterminada del evento
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        url:"../ajax/categoria.php?op=guardaryeditar",
        type:"POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function(datos)
        {
            //bootbox.alert(datos);
            mostrarForm(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}
function mostrar(idcategoria){
    $.post("../ajax/categoria.php?op=mostrar", {idcategoria: idcategoria},function(data,status){
        data = JSON.parse(data);
        mostrarForm(true);

        $("#nombre").val(data.nombre);
        $("#descripcion").val(data.descripcion);
        $("#idcategoria").val(data.idcategoria);

    })
}

function desactivar(idcategoria){
    bootbox.confirm("Esta seguro de desactivar la categoria",function(result){
        if(result){
            $.post("../ajax/categoria.php?op=desactivar",{idcategoria:idcategoria},function(e){
                bootbox.alert(e);
                tabla.ajax.reload();
            });
        }
    })
}
function activar(idcategoria){
    bootbox.confirm("Esta seguro de activar la categoria",function(result){
        if(result){
            $.post("../ajax/categoria.php?op=activar",{idcategoria:idcategoria},function(e){
                bootbox.alert(e);
                tabla.ajax.reload();
            });
        }
    })
}
init();
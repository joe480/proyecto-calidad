var tabla;
function init(){
    mostrarForm(false)
    listar();
}
function mostrarForm(flag){
    //limpiar();
    if(flag){
        $("#listadoRegistros").hide();
        $("#formularioRegistros").show();
        $("#btnGuardar").prop("disabled",false);
        $("#btnagregar").hide();
    }
    else{
        $("#listadoRegistros").show();
        $("#formularioRegistros").hide();
        $("#btnagregar").hide();
    }
}

function listar(){
    tabla=$('#tbllistado').dataTable(
        {
        "aProcessing":true,//Activamos el procesamiento de dataTables
        "aServerSide":true,//Paginacion y filtrado realizados por el servidor
        dom: 'Bfrtip',//Definimos los elementos de control de la tabla
        buttons:[
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":
                {
                    url:'../ajax/permiso.php?op=listar',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "bDestroy":true,
        "iDisplayLength":5,//Paginacion
        "order":[[0,"desc"]]        
    }).DataTable();
}


init();
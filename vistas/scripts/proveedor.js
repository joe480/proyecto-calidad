var tabla;
function init(){
    mostrarForm(false)
    listar();

    $("#formulario").on("submit",function(e){
        guardaryeditar(e);
    })
}
function limpiar(){
    $("#nombre").val("");
    $("#num_documento").val("");
    $("#direccion").val("");
    $("#telefono").val("");
    $("#email").val("");
    $("#idpersona").val("");
}
function mostrarForm(flag){
    limpiar();
    if(flag){
        $("#listadoRegistros").hide();
        $("#formularioRegistros").show();
        $("#btnGuardar").prop("disabled",false);
        $("#btnagregar").hide();
    }
    else{
        $("#listadoRegistros").show();
        $("#formularioRegistros").hide();
        $("#btnagregar").show();
    }
}

function cancelarForm(){
    limpiar();
    mostrarForm(false);
}

function listar(){
    tabla=$('#tbllistado').dataTable(
        {
        "aProcessing":true,//Activamos el procesamiento de dataTables
        "aServerSide":true,//Paginacion y filtrado realizados por el servidor
        dom: 'Bfrtip',//Definimos los elementos de control de la tabla
        buttons:[
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":
                {
                    url:'../ajax/persona.php?op=listarp',
                    type:"get",
                    dataType:"json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "bDestroy":true,
        "iDisplayLength":5,//Paginacion
        "order":[[0,"desc"]]        
    }).DataTable();
}

function guardaryeditar(e){
    e.preventDefault(); //No se activara la accion predeterminada del evento
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        url:"../ajax/persona.php?op=guardaryeditar",
        type:"POST",
        data: formData,
        contentType: false,
        processData: false,

        success: function(datos)
        {
            bootbox.alert(datos);
            mostrarForm(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}
function mostrar(idpersona){
    $.post("../ajax/persona.php?op=mostrar", {idpersona: idpersona},function(data,status){
        data = JSON.parse(data);
        mostrarForm(true);

        $("#nombre").val(data.nombre);
        $("#tipo_documento").val(data.tipo_documento);
        $("#tipo_documento").selectpicker('refresh');
        $("#num_documento").val(data.num_documento);
        $("#direccion").val(data.direccion);
        $("#telefono").val(data.telefono);
        $("#email").val(data.email);
        $("#idpersona").val(data.idpersona);
        

    })
}

function eliminar(idpersona){
    bootbox.confirm("Esta seguro de eliminar el proveedor?",function(result){
        if(result){
            $.post("../ajax/persona.php?op=eliminar",{idpersona:idpersona},function(e){
                bootbox.alert(e);
                tabla.ajax.reload();
            });
        }
    })
}

init();
<?php
require"../config/conexion.php";

Class Venta{
    public function __construct()
    {
        
    }

    public function insertar($idcliente,$idusuario,$tipo_comprobante,$serie_comprobante,
        $num_comprobante,$fecha_hora,$impuesto,$total_venta,$idarticulo,$cantidad,$precio_venta,$descuento){
        $sql = "INSERT INTO venta (idcliente,idusuario,tipo_comprobante,serie_comprobante,
        num_comprobante,fecha_hora,impuesto,total_venta,estado)
        VALUES ('$idcliente','$idusuario','$tipo_comprobante','$serie_comprobante',
        '$num_comprobante','$fecha_hora','$impuesto','$total_venta','Aceptado')";

        $idventanew = ejecutarConsulta_retornarID($sql);
        $num_elementos =0;
        $sw =true;

        while($num_elementos < count($idarticulo)){
            $sql_detalle= "INSERT INTO detalle_venta(idventa,idarticulo,cantidad,precio_venta,
            descuento) VALUES('$idventanew','$idarticulo[$num_elementos]','$cantidad[$num_elementos]',
            '$precio_venta[$num_elementos]','$descuento[$num_elementos]')";
            ejecutarConsulta($sql_detalle) or $sw=false;
            $num_elementos=$num_elementos+1;
        }
        return $sw;
    }

    public function anular($idventa){
        $sql = "UPDATE venta SET estado='Anulado'
        WHERE idventa='$idventa'";
        return ejecutarConsulta($sql);
    }

    public function mostrar($idventa){
        $sql = "SELECT 
        v.idventa,
        DATE(v.fecha_hora) as fecha,
        v.idcliente,
        p.nombre as cliente,
        u.idusuario,
        u.nombre as usuario,
        v.tipo_comprobante,
        v.serie_comprobante,
        v.num_comprobante,
        v.total_venta,
        v.impuesto,
        v.estado
         FROM venta v INNER JOIN persona p ON 
         v.idclienter=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario
         WHERE idventa='$idventa'";
        return ejecutarConsultaSimpleFila($sql);
    }
    public function listarDetalle($idventa){
        $sql =" SELECT 
        dv.idingreso,
        dv.idarticulo,
        dv.cantidad,
        dv.precio_venta,
        dv.descuento,
        (dv.cantidad*dv.precio_venta-dv.descuento) as subtotal,
        a.nombre
        FROM detalle_venta dv INNER JOIN articulo a 
        ON dv.idarticulo=a.idarticulo WHERE dv.idventa='$idventa'" ;
        return ejecutarConsulta($sql);
    }
    public function listar(){
        $sql = "SELECT 
        v.idventa,
        DATE(i.fecha_hora) as fecha,
        v.idcliente,
        p.nombre as cliente,
        u.idusuario,
        u.nombre as usuario,
        v.tipo_comprobante,
        v.serie_comprobante,
        v.num_comprobante,
        v.total_venta,
        v.impuesto,
        v.estado
         FROM venta v INNER JOIN persona p ON 
         v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario ORDER BY v.idventa desc";
        return ejecutarConsulta($sql);
    }
    
}
?>